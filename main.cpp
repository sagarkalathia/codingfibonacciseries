
#include "InfInt.h"
#include <time.h>
#include <cstdlib>
#include <iostream>
// #include <climits>
// #include <limits.h>
using namespace std;
/***********

    As, for n > 93, returned number would need, more than 64 bit precision
    (for unsigned long long), InfInt utility class has been used, which provides precision 
    for larger numbers. 

    *********************************/

static InfInt IterativeFibonacci(int number)
{
    InfInt FibonacciValue = 0, oldVal = 0, olderVal = 0;
    unsigned long long int older = 0, old = 1, sum = 1;
    int limit = number, Iteration = 0;
    
    if(limit < 0)  {return -1;}
    if(limit<2) { 
        FibonacciValue = number; 
        return FibonacciValue;
    }

    if(limit>93)    limit = 93;

    for(Iteration = 2; Iteration <= limit; ++Iteration){
        sum += older;
        older = old;
        old = sum;
    }
    
    FibonacciValue = sum;

    if(number>93){
        
        oldVal = old;
        olderVal = older;

        for(Iteration = 94; Iteration<=number; ++Iteration){
            FibonacciValue += olderVal;
            olderVal = oldVal;
            oldVal = FibonacciValue;
        }

    }
    return FibonacciValue;
}

static InfInt RecursiveFibonacci(int number){
    if(number < 0) { return -1;}
    InfInt FibonacciValue = 0;
    if(number < 2) {
        FibonacciValue = number;
        return FibonacciValue;
    }
    return (RecursiveFibonacci(number-1) + RecursiveFibonacci(number-2));
}

static InfInt RecursiveFibonacciOptimized(int number, InfInt older = 0, InfInt old = 1){
    InfInt FibonacciValue = 0;
    
    if(number>1){   
        return RecursiveFibonacciOptimized(number-1,old,older+old);    
    }
    else if(number == 0){
        return older;
    }
    else if(number == 1){
        return old;  
    }
    else return -1;
}

int main(int argc, const char * argv[])
{   
    int number=94;
    cout << "enter number to find fibonacci value(0-100):";
    cin >> number;
    if(cin.fail() || number<0){
        cout << "invalid\n";
        cin.clear();
	return 0;        
    }
    InfInt FibonacciValue = 0;
    clock_t start = clock();
    FibonacciValue =  IterativeFibonacci(number);
    //FibonacciValue = RecursiveFibonacciOptimized(number);
    //FibonacciValue = RecursiveFibonacci(number);
    clock_t end = clock();
    if(number >= 0){
        cout << "fibonacci(" << number << "): " << FibonacciValue << endl;
    }
    else{
        cout << "Invalid entry\n";
    }
    cout << " time to execute: " << (double) (end - start) / (CLOCKS_PER_SEC) * 1000 << " ms" << endl;

    return 0;
}
