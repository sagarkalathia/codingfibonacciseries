# CodingFibonacciSeries


*  This Project is Implementation of Fibonacci Series in Iterative and Recursive Way.

*  Project contains 3 files

    1.  main.cpp - contains 3 functions of Fibonacci implementation.

    2.  Infint.h - library’s implementation (to handle large precision numbers, more than 64 bit) 

    3.  Answers.txt - answers and explanation of Part I, II & III. 

*  Function takes input n, where 0 <= n <= 100. But It is implemented such that it can handle higher number of inputs as well.


Three functions are written.

1) IterativeFibonacci: 
	-Returns InfInt type number(required for larger precision numbers, for input>93)
	
	Note: Instead of "InfInt" type, GCC's extension of unsigned __int128_t can be used, but as it is target dependent, it has not been used in given code. (assuming all targets might not support integer mode wide enough to hold 128 bits.) 

2) RecursiveFibonacci: 
	-Takes longer to execute for inputs starting from ~35 to 40.

3) RecursiveFibonacciOptimized:
	-apart from input number n, two other parameters as variables are passed to maintain previous two numbers of fibonacci series starting from 0 and 1.
	i.e. RecursiveFibonacciOptimized(number, older = 0, old = 1) and then assigning
	older = old , 
	old = old + older in recursive calls.. Hence recursion is not required to unfold and function can exit on last call.  
